<?php

namespace Shirtplatform\Rma\Controller\Adminhtml\Order\Exchange;

use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order\Reorder\UnavailableProductsProvider;
use Mirasvit\Rma\Api\Repository\RmaRepositoryInterface;

class Start extends \Magento\Sales\Controller\Adminhtml\Order\Create {

    const ADMIN_RESOURCE = 'Mirasvit_Rma::rma_rma';

    /**
     * @var ExtensibleDataObjectConverter
     */
    private $_dataObjectConverter;

    /**
     * @var \Magento\Framework\DataObject\Copy
     */
    private $_objectCopyService;

    /**
     * @var \Shirtplatform\Core\Helper\Order
     */
    private $_orderHelper;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $_quoteRepository;

    /**
     * @var \Mirasvit\Rma\Api\Service\Item\ItemManagementInterface
     */
    private $_rmaItemManagement;

    /**
     * @var \Mirasvit\Rma\Api\Service\Rma\RmaManagementInterface
     */
    private $_rmaManagement;

    /**
     * @var RmaRepositoryInterface
     */
    private $_rmaRepository;

    /**
     * @var Mirasvit\Rma\Api\Service\Rma\RmaManagement\SearchInterface
     */
    private $_rmaSearch;

    /**
     * @var UnavailableProductsProvider
     */
    private $_unavailableProductsProvider;

    /**
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Framework\Escaper $escaper
     * @param PageFactory $resultPageFactory
     * @param ForwardFactory $resultForwardFactory
     * @param ExtensibleDataObjectConverter $dataObjectConverter
     * @param \Magento\Framework\DataObject\Copy $objectCopyService
     * @param \Shirtplatform\Core\Helper\Order $orderHelper
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Mirasvit\Rma\Api\Service\Item\ItemManagementInterface $rmaItemManagement
     * @param \Mirasvit\Rma\Api\Service\Rma\RmaManagementInterface $rmaManagement
     * @param RmaRepositoryInterface $rmaRepository
     * @param \Mirasvit\Rma\Api\Service\Rma\RmaManagement\SearchInterface $rmaSearch
     * @param UnavailableProductsProvider $unavailableProductsProvider
     */
    public function __construct(\Magento\Backend\App\Action\Context $context,
                                \Magento\Catalog\Helper\Product $productHelper,
                                \Magento\Framework\Escaper $escaper,
                                PageFactory $resultPageFactory,
                                ForwardFactory $resultForwardFactory,
                                ExtensibleDataObjectConverter $dataObjectConverter,
                                \Magento\Framework\DataObject\Copy $objectCopyService,
                                \Shirtplatform\Core\Helper\Order $orderHelper,
                                \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
                                \Mirasvit\Rma\Api\Service\Item\ItemManagementInterface $rmaItemManagement,
                                \Mirasvit\Rma\Api\Service\Rma\RmaManagementInterface $rmaManagement,
                                RmaRepositoryInterface $rmaRepository,
                                \Mirasvit\Rma\Api\Service\Rma\RmaManagement\SearchInterface $rmaSearch,
                                UnavailableProductsProvider $unavailableProductsProvider
    ) {
        parent::__construct($context, $productHelper, $escaper, $resultPageFactory, $resultForwardFactory);
        $this->_dataObjectConverter = $dataObjectConverter;
        $this->_objectCopyService = $objectCopyService;
        $this->_orderHelper = $orderHelper;
        $this->_quoteRepository = $quoteRepository;
        $this->_rmaItemManagement = $rmaItemManagement;
        $this->_rmaManagement = $rmaManagement;
        $this->_rmaRepository = $rmaRepository;
        $this->_rmaSearch = $rmaSearch;
        $this->_unavailableProductsProvider = $unavailableProductsProvider;
    }

    public function execute() {
        $this->_getSession()->clearStorage();
        $rmaId = $this->getRequest()->getParam('rma_id');

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        try {
            $rma = $this->_rmaRepository->get($rmaId);
        } catch (NoSuchEntityException $ex) {
            $this->messageManager->addErrorMessage(sprintf('RMA %s not found', $rmaId));
            $resultRedirect->setPath('rma/rma/index');
            return $resultRedirect;
        }

        try {
            $order = $this->_rmaManagement->getOrder($rma);
        } catch (NoSuchEntityException $ex) {
            $this->messageManager->addErrorMessage(sprintf('Order for RMA %s not found', $rmaId));
            $resultRedirect->setPath('rma/rma/index');
            return $resultRedirect;
        }

        $exchangeItems = [];
        $rmaItems = $this->_rmaSearch->getItems($rma);

        foreach ($rmaItems as $_rmaItem) {
            if ($this->_rmaItemManagement->isExchange($_rmaItem)) {
                $exchangeItems[] = $_rmaItem;
            }
        }

        $unavailableProducts = $this->_unavailableProductsProvider->getForOrder($order);
        foreach ($exchangeItems as $key => $_rmaItem) {
            if (in_array($_rmaItem->getProductSku(), $unavailableProducts)) {
                unset($exchangeItems[$key]);
                $this->messageManager->addErrorMessage(sprintf('Product %s is no longer available', $_rmaItem->getProductSku()));
            }
        }

        if (count($exchangeItems)) {
            $this->_getSession()->setUseOldShippingMethod(true);
            $this->_eventManager->dispatch('shirtplatform_rma_init_from_order_before', ['order' => $order]);
            $this->_initFromOrderAndRma($order, $rma, $exchangeItems);
            $this->_eventManager->dispatch('shirtplatform_rma_init_from_order_after', ['order' => $order]);
        }

        $resultRedirect->setPath('sales/order_create/index', [
            'customer_id' => $rma->getCustomerId(),
            'store_id' => $rma->getStoreId(),
            'rma_id' => $rma->getId()
        ]);
        return $resultRedirect;
    }

    /**
     * Init quote from admin order and RMA. Add only exchange items to quote
     * 
     * @access private
     * @param \Magento\Sales\Model\Order $order
     * @param \Mirasvit\Rma\Api\Data\Rma $rma
     * @param []\Mirasvit\Rma\Model\Item $exchangeItems
     */
    private function _initFromOrderAndRma($order,
                                          $rma,
                                          $exchangeItems) {
        $session = $this->_getSession();
        $session->setCurrencyId($order->getOrderCurrencyCode());
        /* Check if we edit guest order */
        $session->setCustomerId($rma->getCustomerId() ?: false);
        $session->setStoreId($rma->getStoreId());

        $orderCreateModel = $this->_getOrderCreateModel();
        $orderCreateModel->initRuleData();

        foreach ($exchangeItems as $rmaItem) {
            $orderItem = $this->_rmaItemManagement->getOrderItem($rmaItem);
            $orderCreateModel->initFromOrderItem($orderItem, $rmaItem->getQtyRequested());
        }

        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress) {
            $shippingAddress->setSameAsBilling($this->_orderHelper->areAddressesEqual($order));
        }

        $this->_initBillingAddressFromOrder($order);
        $this->_initShippingAddressFromOrder($order);

        $quote = $this->_getOrderCreateModel()->getQuote();
        if (!$quote->isVirtual() && $orderCreateModel->getShippingAddress()->getSameAsBilling()) {
            $orderCreateModel->setShippingAsBilling(1);
        }

        if (!$quote->isVirtual()) {
            $orderCreateModel->setShippingMethod($order->getShippingMethod());
            $quote->getShippingAddress()->setShippingDescription($order->getShippingDescription());
        }

        $this->_objectCopyService->copyFieldsetToTarget('sales_copy_order', 'to_edit', $order, $quote);
        $this->_eventManager->dispatch('sales_convert_order_to_quote', ['order' => $order, 'quote' => $quote]);

        if (!$rma->getCustomerId()) {
            $quote->setCustomerIsGuest(true);
        }

        if (!$quote->isVirtual()) {
            $orderCreateModel->collectShippingRates();
            $quote->getShippingAddress()->unsCachedItemsAll();
        }

        $quote->setTotalsCollectedFlag(false);
        $this->_quoteRepository->save($quote);

        if (!$quote->isVirtual()) {
            //set correct same_as_billing for shipping address, because Magento\Quote\Model\Quote\Address::beforeSave() sets it incorrectly            
            $shippingSameAsBilling = $this->_orderHelper->areAddressesEqual($order);
            $quote->getShippingAddress()->setSameAsBilling($shippingSameAsBilling);
            $this->_quoteRepository->save($quote);
        }
    }

    /**
     * Copy billing address from order
     *
     * @access private
     * @param \Magento\Sales\Model\Order $order
     * @return void
     */
    private function _initBillingAddressFromOrder($order) {
        $quote = $this->_getOrderCreateModel()->getQuote();
        $quote->getBillingAddress()->setCustomerAddressId('');
        $this->_objectCopyService->copyFieldsetToTarget(
                'sales_copy_order_billing_address', 'to_order', $order->getBillingAddress(), $quote->getBillingAddress()
        );
    }

    /**
     * Copy shipping address from order
     *
     * @access private
     * @param \Magento\Sales\Model\Order $order
     * @return void
     */
    private function _initShippingAddressFromOrder($order) {
        if (!$order->getIsVirtual()) {
            $quote = $this->_getOrderCreateModel()->getQuote();
            $orderShippingAddress = $order->getShippingAddress();
            $quoteShippingAddress = $quote->getShippingAddress()->setCustomerAddressId('')
                    ->setSameAsBilling($orderShippingAddress && $orderShippingAddress->getSameAsBilling());
            $this->_objectCopyService->copyFieldsetToTarget(
                    'sales_copy_order_shipping_address', 'to_order', $orderShippingAddress, $quoteShippingAddress
            );
        }
    }
}
