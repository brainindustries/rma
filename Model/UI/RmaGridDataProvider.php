<?php

namespace Shirtplatform\Rma\Model\UI;

use Mirasvit\Rma\Api\Config\OfflineOrderConfigInterface;
use Mirasvit\Rma\Api\Service\Field\FieldManagementInterface;
use Mirasvit\Rma\Api\Config\FieldConfigInterface as FieldConfig;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;

use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Serialize\Serializer\Serialize;

class RmaGridDataProvider extends \Mirasvit\Rma\Model\UI\Rma\RmaGridDataProvider 
{
    /*
     * Magento JSON serializer
     * @var object
     */
    private $_jsonSerializer;    
    
    
    /*
     * Magento serializer
     * @var object
     */
    private $_serializer;
    
    
    /**
     * Constructor
     *
     * @param SerializerInterface $jsonSerializer
     * @param Serialize $serialize
     */
    public function __construct(
        SerializerInterface $jsonSerializer, 
        Serialize $serialize,
        OfflineOrderConfigInterface $offlineConfig,
        FieldManagementInterface $fieldManagement,
        $name,
        $primaryFieldName,
        $requestFieldName,
        ReportingInterface $reporting,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RequestInterface $request,
        FilterBuilder $filterBuilder,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($offlineConfig, $fieldManagement, $name, $primaryFieldName, $requestFieldName, 
                $reporting, $searchCriteriaBuilder, $request, $filterBuilder, $meta, $data);
        
        $this->_jsonSerializer = $jsonSerializer;
        $this->_serializer = $serialize;
    }    

    
    /**
     * Unserialize JSON
     *
     * @param string $string
     * @return string
     */
    private function _unserializeJSON($string){
        try {
            $result = $this->_jsonSerializer->unserialize($string)[0];
        } catch (\Exception $ex) {
            $result = $string;
        }
        return $result;
    }
    
    
    /**
     * Unserialize array
     *
     * @param string $string
     * @return string
     */
    private function _unserializeArray($string){
        try {
            $result = $this->_serializer->unserialize($string)[0];
        } catch (\Exception $ex) {
            $result = $string;
        }
        return $result;
    }
    
    
    /**
     * Returns Search result. Add platform order id
     *
     * @access public
     * @return \Mirasvit\Rma\Model\ResourceModel\Rma\Collection
     */
    public function getSearchResult() {
        $collection = parent::getSearchResult();
        
        $orderTable = $collection->getTable('sales_order');
        $collection->join(['order_table' => $orderTable], 'main_table.order_id = order_table.entity_id', ['shirtplatform_id AS platform_order_id']);
        
        $rmaItemTable = $collection->getTable('mst_rma_item');
        $rmaReasonTable = $collection->getTable('mst_rma_reason');
        $collection->getSelect()
                ->join(['item_table' => $rmaItemTable], 'main_table.rma_id = item_table.rma_id', [])
                ->join(['reason_table' => $rmaReasonTable], 'item_table.reason_id = reason_table.reason_id', ['name AS rma_reason'])
                ->group('main_table.rma_id');
        
        foreach ($collection->getItems() as $item){
            $reason = $item->getRmaReason();
            $reason = $this->_unserializeJSON($reason);
            $reason = $this->_unserializeArray($reason);
            $item->setRmaReason($reason);
        }
        
        return $collection;
    }

}
