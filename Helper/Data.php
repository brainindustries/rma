<?php

namespace Shirtplatform\Rma\Helper;

use Magento\Framework\App\Helper\Context;
use Mirasvit\Rma\Api\Service\Item\ItemManagementInterface;
use Mirasvit\Rma\Repository\RmaRepository;
use Shirtplatform\Core\Helper\Image;
use Shirtplatform\Core\Model\Config\Source\ProductType;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    /**
     * Storage of order items
     *      
     * @var array
     */
    private $_orderItems = [];

    /**
     * @var ItemManagementInterface
     */
    private $_rmaItemManagement;

    /**
     * @var RmaRepository
     */
    private $_rmaRepository;

    /**
     * @var Image
     */
    private $_shirtplatformImageHelper;

    /**
     * 
     * @param Context $context
     * @param Image $shirtplatformImageHelper
     * @param ItemManagementInterface $rmaItemManagement
     * @param RmaRepository $rmaRepository
     */
    public function __construct(
        Context $context,
        Image $shirtplatformImageHelper,
        ItemManagementInterface $rmaItemManagement,
        RmaRepository $rmaRepository
    ) {
        parent::__construct($context);
        $this->_rmaItemManagement = $rmaItemManagement;
        $this->_rmaRepository = $rmaRepository;
        $this->_shirtplatformImageHelper = $shirtplatformImageHelper;
    }

    /**
     * Get order item for rma item
     * 
     * @access public
     * @param \Mirasvit\Rma\Api\Data\ItemInterface $rmaItem
     * @return \Magento\Sales\Api\Data\OrderItemInterface
     */
    public function getOrderItem($rmaItem) {
        if (!isset($this->_orderItems[$rmaItem->getOrderItemId()])) {
            $orderItem = $this->_rmaItemManagement->getOrderItem($rmaItem);
            $this->_orderItems[$rmaItem->getOrderItemId()] = $orderItem;
        }

        return $this->_orderItems[$rmaItem->getOrderItemId()];
    }

    /**
     * Does rma item represent shirtplatform base product?
     * 
     * @access public
     * @param \Mirasvit\Rma\Api\Data\ItemInterface $rmaItem
     * @return boolean
     */
    public function isShirtplatformBaseProduct($rmaItem) {
        $orderItem = $this->getOrderItem($rmaItem);

        if ($orderItem) {
            return $orderItem->getShirtplatformProductType() == ProductType::BASE_PRODUCT;
        }

        return false;
    }

    /**
     * Does rma item represent shirtplatform collection product?
     * 
     * @access public
     * @param \Mirasvit\Rma\Api\Data\ItemInterface $rmaItem
     * @return boolean
     */
    public function isShirtplatformCollectionProduct($rmaItem) {
        $orderItem = $this->getOrderItem($rmaItem);

        if ($orderItem) {
            return $orderItem->getShirtplatformProductType() == ProductType::COLLECTION_PRODUCT;
        }

        return false;
    }

    /**
     * Get images with designs
     * 
     * @access public
     * @param \Mirasvit\Rma\Api\Data\ItemInterface $rmaItem
     * @param array $dimensions (width, height)
     * @return array
     */
    public function getDesignImages($rmaItem,
                                    $dimensions = []) {
        $images = [];

        if (!isset($dimensions['width'])) {
            $dimensions['width'] = 70;
        }
        if (!isset($dimensions['height'])) {
            $dimensions['height'] = 70;
        }

        $orderItem = $this->getOrderItem($rmaItem);

        if ($orderItem) {
            $images = $this->_shirtplatformImageHelper->getDesignImages($orderItem, $dimensions);
        }

        return $images;
    }

    /**
     * Get images with designs
     * 
     * @access public
     * @param \Mirasvit\Rma\Api\Data\ItemInterface $rmaItem
     * @param array $dimensions (width, height)
     * @param string $imageId image type
     * @return array
     */
    public function getCollectionImages($rmaItem,
                                        $dimensions = [],
                                        $imageId = 'small_image') {
        $images = [];

        if (!isset($dimensions['width'])) {
            $dimensions['width'] = 70;
        }
        if (!isset($dimensions['height'])) {
            $dimensions['height'] = 70;
        }

        $orderItem = $this->getOrderItem($rmaItem);

        if ($orderItem) {
            $images = $this->_shirtplatformImageHelper->getCollectionImages($orderItem, $dimensions, $imageId);
        }

        return $images;
    }

    /**
     * @param int $orderId
     * @return bool
     */
    public function orderIsReclamation($orderId) {
        $collection = $this->_rmaRepository->getCollection()->addFieldToFilter('rma_order.re_exchange_order_id', $orderId);
        $collection->getSelect()->joinLeft(['rma_order' => 'mst_rma_rma_order'], 'main_table.rma_id = rma_order.re_rma_id');
        $order = $collection->getFirstItem();
        return (bool)$order->getData();
    }

}
