<?php

namespace Shirtplatform\Rma\Observer\Backend;

use Magento\Framework\Event\ObserverInterface;

class ConvertOrderItemToQuoteItem implements ObserverInterface {

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $_request;

    /**
     * 
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(\Magento\Framework\App\RequestInterface $request) {
        $this->_request = $request;
    }

    /**
     * Set price as 0 for exchange order quote items
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $actionName = $this->_request->getFullActionName();

        if ($actionName == 'shirtplatform_rma_order_exchange_start') {
            $quoteItem = $observer->getQuoteItem();
            $quoteItem->setCustomPrice(0);
            $quoteItem->setOriginalCustomPrice(0);
            $quoteItem->getProduct()->setIsSuperMode(true);
        }
    }

}
