<?php

namespace Shirtplatform\Rma\Observer\Backend\Sales\Order\Edit;

use Magento\Framework\Event\ObserverInterface;

class UpdateRmaItems implements ObserverInterface {
    
    /**     
     * @var \Mirasvit\Rma\Api\Repository\ItemRepositoryInterface
     */
    private $_rmaItemRepository;
    
    /**
     * 
     * @param \Mirasvit\Rma\Api\Repository\ItemRepositoryInterface $rmaItemRepository
     */
    public function __construct(\Mirasvit\Rma\Api\Repository\ItemRepositoryInterface $rmaItemRepository) {
        $this->_rmaItemRepository = $rmaItemRepository;
    }
    
    /**
     * Register before commit callback that will update order_item_id for rma items
     * with the new IDs
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $transaction = $observer->getTransaction();                
        $self = $this;
        $transaction->addCommitCallback(function() use($self, $observer) {                        
            $self->_updateOrderItemIds($observer->getOrder(), $observer->getItemMapper());
        });        
    }
    
    /**
     * Update order_item_id for rma items when order is edited
     * 
     * @access private
     * @param \Magento\Sales\Model\Order $order
     * @param array $quoteItemToOrderItemMapper
     */
    private function _updateOrderItemIds($order, $quoteItemToOrderItemMapper) {        
        $rmaItems = $this->_rmaItemRepository->getCollection()
                ->addFieldToFilter('order_item_id', $quoteItemToOrderItemMapper);

        if (count($rmaItems)) {
            $oldItemIdToNewItemIdMapper = [];
            
            foreach ($order->getAllItems() as $orderItem) {
                if (isset($quoteItemToOrderItemMapper[$orderItem->getQuoteItemId()])) {
                    $oldItemId = $quoteItemToOrderItemMapper[$orderItem->getQuoteItemId()];
                    $oldItemIdToNewItemIdMapper[$oldItemId] = $orderItem->getId();
                }
            }

            foreach ($rmaItems as $_rmaItem) {
                if (isset($oldItemIdToNewItemIdMapper[$_rmaItem->getOrderItemId()])) {
                    $newItemId = $oldItemIdToNewItemIdMapper[$_rmaItem->getOrderItemId()];
                    $_rmaItem->setOrderItemId($newItemId);
                    $this->_rmaItemRepository->save($_rmaItem);
                }
            }
        }        
    }
}