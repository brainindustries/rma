<?php

namespace Shirtplatform\Rma\Plugin\Service\Item\ItemManagement;

class Quantity
{
    /**
     * RMA items group by product SKU their requested quantities to be returned
     */
    private $_requestedQuantities = [];

    /**
     * @var \Mirasvit\Rma\Api\Service\Item\ItemManagementInterface
     */
    private $_itemManagement;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $_orderRepository;

    /**
     * @var \Mirasvit\Rma\Api\Service\Rma\RmaManagementInterface
     */
    private $_rmaManagement;

    /**
     * @var \Mirasvit\Rma\Api\Service\Rma\RmaManagement\SearchInterface
     */
    private $_rmaSearchManagement;

    /**
     * 
     * @access public
     * @param \Mirasvit\Rma\Api\Service\Item\ItemManagementInterface $itemManagement
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Mirasvit\Rma\Api\Service\Rma\RmaManagementInterface $rmaManagement
     * @param \Mirasvit\Rma\Api\Service\Rma\RmaManagement\SearchInterface $rmaSearchManagement     
     */
    public function __construct(
        \Mirasvit\Rma\Api\Service\Item\ItemManagementInterface $itemManagement,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Mirasvit\Rma\Api\Service\Rma\RmaManagementInterface $rmaManagement,
        \Mirasvit\Rma\Api\Service\Rma\RmaManagement\SearchInterface $rmaSearchManagement
    ) {
        $this->_itemManagement = $itemManagement;
        $this->_orderRepository = $orderRepository;
        $this->_rmaManagement = $rmaManagement;
        $this->_rmaSearchManagement = $rmaSearchManagement;
    }

    /**
     * Return array that maps product SKU to their requested quantity for
     * all RMAs that belong to the original order
     * 
     * @access private
     * @param \Mirasvit\Rma\Api\Data\ItemInterface $rmaItem
     * 
     * @return array
     */
    private function _getRequestedQuantities($rmaItem)
    {
        if (empty($this->_requestedQuantities)) {
            $orderItem = $this->_itemManagement->getOrderItem($rmaItem);
            $order = $this->_orderRepository->get($orderItem->getOrderId());
            $rmas = $this->_rmaManagement->getRmasByOrder($order);            

            foreach ($rmas as $_rma) {
                $requestedItems = $this->_rmaSearchManagement->getRequestedItems($_rma);

                foreach ($requestedItems as $reqItem) {
                    if (!isset($this->_requestedQuantities[$reqItem->getProductSku()])) {
                        $this->_requestedQuantities[$reqItem->getProductSku()] = $reqItem->getQtyRequested();
                    }
                    else {
                        $this->_requestedQuantities[$reqItem->getProductSku()] += $reqItem->getQtyRequested();
                    }
                }
            }
        }

        return $this->_requestedQuantities;
    }

    /**
     * Solves the performance issue for the original method
     * 
     * @access public
     * @param \Mirasvit\Rma\Service\Item\ItemManagement\Quantity $subject
     * @param \Closure $proceed
     * @param \Mirasvit\Rma\Api\Data\ItemInterface $item
     * 
     * @return int
     */
    public function aroundGetItemQtyReturned($subject, $proceed, \Mirasvit\Rma\Api\Data\ItemInterface $item)
    {        
        $requestedQuantities = $this->_getRequestedQuantities($item);
        if (isset($requestedQuantities[$item->getProductSku()])) {
            return $requestedQuantities[$item->getProductSku()];
        }

        return 0;
    }
}
