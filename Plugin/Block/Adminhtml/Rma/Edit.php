<?php

namespace Shirtplatform\Rma\Plugin\Block\Adminhtml\Rma;

class Edit {

    /**
     * Return our own exchange order URL that will initialize the products for
     * the exchange order
     * 
     * @access public
     * @param \Mirasvit\Rma\Block\Adminhtml\Rma\Edit $subject
     * @param \Closure $proceed
     * @param \Mirasvit\Rma\Model\Rma $rma
     * @return string
     */
    public function aroundGetCreateOrderUrl($subject,
                                            $proceed,
                                            $rma) {
        return $subject->getUrl(
                        'shirtplatform_rma/order_exchange/start', [
                    'customer_id' => $rma->getCustomerId(),
                    'store_id' => $rma->getStoreId(),
                    'rma_id' => $rma->getId()
                        ]
        );
    }

}
