<?php

namespace Shirtplatform\Rma\Plugin\Block\Adminhtml\Rma\Edit;

class Form {
    
    /**     
     * @var \Mirasvit\Rma\Api\Service\Rma\RmaManagementInterface 
     */
    private $_rmaManagement;
    
    /**
     * 
     * @param \Mirasvit\Rma\Api\Service\Rma\RmaManagementInterface $rmaManagement
     */
    public function __construct(\Mirasvit\Rma\Api\Service\Rma\RmaManagementInterface $rmaManagement) {
        $this->_rmaManagement = $rmaManagement;
    }
    
    /**
     * Change block and template of items for our own
     * 
     * @access public
     * @param \Mirasvit\Rma\Block\Adminhtml\Rma\Edit\Form $subject
     * @param \Closure $proceed
     * @return string
     */
    public function aroundGetItemsHtml($subject, $proceed) {
        $rma = $subject->getRma();
        $template = 'Shirtplatform_Rma::rma/edit/form/items.phtml';
        $order = $this->_rmaManagement->getOrder($rma);        
        if (!$order) {
            $template = 'Mirasvit_Rma::rma/edit/form/offline/create_item.phtml';
        } elseif ($order->getIsOffline()) {
            $template = 'Mirasvit_Rma::rma/edit/form/offline/items.phtml';
        }

        return $subject->getLayout()->createBlock(\Mirasvit\Rma\Block\Adminhtml\Rma\Edit\Form\Items::class)
            ->setRma($rma)
            ->setOrder($order)
            ->setTemplate($template)
            ->toHtml();
    }
}