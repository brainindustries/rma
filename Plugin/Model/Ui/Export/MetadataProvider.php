<?php

namespace Shirtplatform\Rma\Plugin\Model\Ui\Export;

class MetadataProvider {

    /**
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    private $_filter;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $_orderRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $_storeManager;
    
    /**
     * Stores data
     * 
     * @var array
     */
    private $_stores = [];

    /**
     * 
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(\Magento\Ui\Component\MassAction\Filter $filter,
                                \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
                                \Magento\Store\Model\StoreManagerInterface $storeManager) {
        $this->_filter = $filter;
        $this->_orderRepository = $orderRepository;
        $this->_storeManager = $storeManager;
        $this->_stores = $this->_storeManager->getStores();
    }

    /**
     * Add store information and order increment_id
     * 
     * @access public
     * @param \Magento\Ui\Model\Export\MetadataProvider $subject
     * @param array $result
     * @return array
     */
    public function afterGetRowData($subject,
                                    $result) {
        $component = $this->_filter->getComponent();

        if ($component->getName() == 'rma_listing') {
            $fields = array_flip($subject->getFields($component));
            
            if (isset($fields['store_id'])) {
                $key = $fields['store_id'];
                $storeId = $result[$key];
                
                if ($storeId and isset($this->_stores[$storeId])) {
                    $store = $this->_stores[$storeId];
                    $purchasePoint = $store->getWebsite()->getName() . ' -> ' . $store->getGroup()->getName() . ' -> ' . $store->getName();
                    $result[$key] = $purchasePoint;                    
                }
            }
            
            if (isset($fields['order_id'])) {
                $key = $fields['order_id'];
                $orderId = $result[$key];
                
                if ($orderId) {
                    $order = $this->_orderRepository->get($orderId);
                    $result[$key] = $order->getIncrementId();
                }
                
            }            
        }

        return $result;
    }

}
