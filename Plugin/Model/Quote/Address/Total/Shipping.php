<?php

namespace Shirtplatform\Rma\Plugin\Model\Quote\Address\Total;

class Shipping {

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $_request;

    /**
     * @var \Magento\Backend\Model\Session\Quote
     */
    private $_session;

    /**
     * 
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Backend\Model\Session\Quote $session
     */
    public function __construct(\Magento\Framework\App\RequestInterface $request,
                                \Magento\Backend\Model\Session\Quote $session) {
        $this->_request = $request;
        $this->_session = $session;
    }

    /**
     * Set custom shipping price to 0 for exchange orders
     * 
     * @access public
     * @param \Magento\Quote\Model\Quote\Address\Total\Shipping $subject
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return array
     */
    public function beforeCollect($subject,
                                  $quote,
                                  $shippingAssignment,
                                  $total) {
        $actionName = $this->_request->getFullActionName();
        
        if ($actionName == 'shirtplatform_rma_order_exchange_start') {            
            $this->_session->setCustomShippingPrice(0);            
        }
        
        return [$quote, $shippingAssignment, $total];
    }

}
